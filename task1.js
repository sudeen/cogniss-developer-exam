// Task to be done
// load names array from JSON file input2.json
//for each name reverse it append 5 random characters plus @gmail.com
// end result should be fake email addresses
// Save the array as the property emails into a JSON file called output2.json.

const jsonfile = require("jsonfile");
var randomstring = require("randomstring");

const inputFile = "./input2.json";
const outputFile = "./output2.json";

// Generate Random String
const generateRandomString = (input) => {
  console.log("Random String: ", randomstring.generate(5));
  return input + randomstring.generate(5);
};

// File Write
const createJson = (obj) => {
  jsonfile
    .writeFile(outputFile, obj)
    .then((response) => {
      console.log("File Created");
    })
    .catch((error) => console.error(error));
};

//Read JSON File
jsonfile
  .readFile(inputFile)
  .then((getListOfName) => fakeEmailAddresses(getListOfName))
  .catch((error) => console.error(error));

// Create Fake Emails addresses
const fakeEmailAddresses = ({ names }) => {
  const emails = {
    // Map function is used to access each array element
    emails: names.map(
      (name) =>
        `${generateRandomString(
          name.split("").reverse().join(""),
          5
        )}@gmail.com`
    ),
  };
  console.log("Created Fake Emails: ", emails);
  createJson(emails);
};
